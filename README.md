# CREDBANK Extract Tweets IDs

Source code to extract Tweets IDs from CREDBANK dataset, more specifically from cred_event_SearchTweets.data file.

Each line in the file contains 4 fields:

* ```topic_key``` : same as in (2). Credbility Annotation File
* ```topic_terms``` : same as in (2). Credbility Annotation File
* ```tweet_count```: Total number of tweets returned by the search API for the topic_terms.
* ```ListOf_tweetid_author_createdAt_tuple``` : A list of tuples, where each tuple contains three fields: ```tweet ID``` , ```tweet author``` , ```tweet creation date```  

A snippet:

```
topic_key	topic_terms	tweet_count	ListOf_tweetid_author_createdAt_tuple
louis_ebola_nurse-20141024_170629-20141024_181626       [u'louis', u'ebola', u'nurse']  13243 [('ID=522760301435830272', 'AUTHOR=iMhartyz', 'CreatedAt=2014-10-16 14:45:42'), ('ID=522760172872413184', 'AUTHOR=Tino_carter_v', 'CreatedAt=2014-10-16 14:45:12'), ......] 

```

More information access: [(CREDBANK)](https://github.com/compsocial/CREDBANK-data)