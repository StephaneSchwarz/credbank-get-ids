# --- PACKAGE IMPORTATION ---#

import pandas as pd
import numpy as np
import argparse
import json
import sys
import re

""" Get the code arguments

Returns:
	
	list: A list of arguments
"""
def parse_arguments():

	parser = argparse.ArgumentParser(description = __doc__)

	parser.add_argument('--dataset-file-name', '-f',
						dest='dataset_file_name',
						required=True,
						help='Is expected cred_event_SearchTweets.data file.')

	parser.add_argument('--output-csv', '-o',
						dest='output_csv_file',
						required=True,
						help='The path and the file name to store the output file. Ex.: ./tweets_ids.csv')

	return parser.parse_args()

""" Generate the new csv file after extract the tweets IDs

Returns:
	
	None
"""
def extract_tweet_ID_from_file(file_name, output_csv_file):

	tweets_ids = []
	topic_key_list = []
	
	i = 0

	# Open dataset
	with open(file_name) as file:
		
		header = file.readline()
		
		for row in file:

			splited_row = row.split("\t")

			topic_key = splited_row[0]

			#topic_terms = splited_row[1]
			#tweet_count = splited_row[2]

			tweet_tuple = splited_row[3]

			tweet_tuple = re.sub(r'\(', '{', tweet_tuple)
			tweet_tuple = re.sub(r'\)', '}', tweet_tuple)
			tweet_tuple = re.sub(r'\'', '\"', tweet_tuple)
			tweet_tuple = re.sub(r'=', '\":\"', tweet_tuple)

			tweets = json.loads(tweet_tuple)

			for tweet in tweets:

				tweets_ids.append(tweet["ID"])
				topic_key_list.append(topic_key)

				# A poor way to see the process loading
				if((i % 100000) == 0):

					print(i)

				i = i + 1

		
		dataset = pd.DataFrame({'topic_key':topic_key_list,'tweet_id':tweets_ids})

		dataset.to_csv(output_csv_file)

		print("Number of tweets: " + str(i))
		print("Number of collected data: " + str(len(tweets_ids)))

		file.close()

		return

if __name__ == '__main__':
	
	args = parse_arguments()

	extract_tweet_ID_from_file(args.dataset_file_name, args.output_csv_file)

